# UI-Testing mit Cypress

## Was ist Cypress?

- Framework für das Testing von Web-Anwendungen
- Open-Source
- Ähnlich wie Selenium
- Meiner Meinung nach deutlich besser, einfacher und flexibler

## Installation

```
npm install cypress --save-dev
```

## Cypress Beispiele

![Cypress Beispiele](images/examples.PNG)

## Der erste Test

```typescript
describe('UITest', async (): Promise<void> => {

    it('UITest', async (): Promise<void> => {

        cy.visit('http://localhost:8080');

        cy.contains('UI-Testing mit Cypress');

        cy.get("[data-cy=auswahl]").parent().click();
        cy.get(".v-menu__content").contains("Auswahl 3").click();

        cy.get('[data-cy=wert]').type('Hello World');

        cy.get('button#speichern').click();

        cy.get('[data-cy=result] tbody tr').each(($el) => {
            cy.wrap($el).within(() => {
                cy.get('td').eq(1).should('have.text', 'Hello World')
            })
        })


    });

});
```

## Mocking HTTP-Requests

### Test

```typescript
describe('Mocking', async (): Promise<void> => {

    beforeEach(() => {
      cy.intercept('GET', 'http://localhost:58000/examples', { fixture: 'data.json' }).as('examples');
    });

    it('Mocking', async (): Promise<void> => {

        cy.visit('http://localhost:8080');

        cy.contains('UI-Testing mit Cypress');

        cy.get('button#backend').click();

        cy.wait(['@examples']);

        cy.get('[data-cy=result] tbody tr').each(($el) => {
            cy.wrap($el).within(() => {
                cy.get('td').eq(1).should('have.text', 'Mocking Request')
            })
        })

    });

});
```

### data.json

```json
[
  {
    "auswahl": "Auswahl 1",
    "wert": "Mocking Request"
  }
]
```

## Vue Components testen

### Vue Component
```vue
<template>
  <section>
    <h1 data-testid="title">👋 Hello {{ name }}!</h1>
    <p>This is your first component tested using browser-based component testing with Cypress + Vue ✌️🌲</p>
  </section>
</template>

<script lang="ts">
import { Component, Prop, Vue } from 'vue-property-decorator';

@Component
export default class Greetings extends Vue {

  @Prop({default: 'world'})
  name!: string;

}
</script>
```

### Test
```typescript
/// <reference types="cypress" />
import { mount } from '@cypress/vue'
import Greetings from '@/components/Greetings.vue'

describe('Greetings', () => {
  it('renders the component with a default name', () => {
    mount(Greetings)

    // now we can use any Cypress command to interact with the component
    // https://on.cypress.io/api
    cy.get('[data-testid=title]').should('exist').and('contain', 'Hello world')
  })

  it('renders the name passed in', () => {
    const name = 'Denise'
    mount(Greetings, {
      propsData: {
        name
      }
    })

    cy.get('[data-testid=title]').should('contain', `Hello ${name}`)
  })
})

```

## UI-Test ausführen

```
cypress open
```

### Dashboard

![Cypress Dashboard](images/dashboard.PNG)

### Ergebnis

![Cypress Ergebnis](images/testresult.PNG)

## Repository

Das komplette Projekt inkl. Cypress UI-Tests steht auf Gitlab zur Verfügung.

[https://gitlab.com/fh-swf/fb-in/moderne_web-frameworks/uitesting_cypress](https://gitlab.com/fh-swf/fb-in/moderne_web-frameworks/uitesting_cypress)
