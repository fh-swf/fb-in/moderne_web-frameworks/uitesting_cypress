describe('Mocking', async (): Promise<void> => {

    beforeEach(() => {
      cy.intercept('GET', 'http://localhost:58000/examples', { fixture: 'data.json' }).as('examples');
    });

    it('Mocking', async (): Promise<void> => {

        cy.visit('http://localhost:8080');

        cy.contains('UI-Testing mit Cypress');

        cy.get('button#backend').click();

        cy.wait(['@examples']);

        cy.get('[data-cy=result] tbody tr').each(($el) => {
            cy.wrap($el).within(() => {
                cy.get('td').eq(1).should('have.text', 'Mocking Request')
            })
        })

    });

});
